'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Vehicles extends Model {
    static associate(models) {
      Vehicles.belongsTo(models.User_Games, { foreignKey: 'User_GameId' })
    }
  };
  Vehicles.init({
    Name: DataTypes.STRING,
    User_GameId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Vehicles',
  });
  return Vehicles;
};