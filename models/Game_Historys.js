'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Game_Historys extends Model {
    static associate(models) {
      Game_Historys.belongsTo(models.Game_Biodatas)
      Game_Historys.belongsTo(models.User_Games)
      // Game_Historys.hasMany(models.User_Games, { foreignKey: "User_GamesId" })
      // Game_Historys.hasMany(models.Game_Biodatas, { foreignKey: "Game_BiodatasId" })
      // define association here
    }
  };
  Game_Historys.init({
    Game_BiodataId: DataTypes.INTEGER,
    User_GameId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Game_Historys',
  });
  return Game_Historys;
};