'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_Games extends Model {
    static associate(models) {
      User_Games.hasOne(models.Class)
      User_Games.hasMany(models.Vehicles)
      // User_Games.hasMany(models.Game_Historys)
      User_Games.belongsToMany(models.Game_Biodatas, { through: "Game_Historys" })

    }
  };
  User_Games.init({
    Name: DataTypes.STRING,
    Age: DataTypes.INTEGER,
    FullName: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'User_Games',
  });
  return User_Games;
};