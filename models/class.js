'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Class extends Model {
    static associate(models) {
      Class.belongsTo(models.User_Games, { foreignKey: 'User_GameId' })
      // define association here
    }
  };
  Class.init({
    name: DataTypes.STRING,
    User_GameId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Class',
  });
  return Class;
};