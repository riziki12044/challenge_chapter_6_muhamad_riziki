'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Game_Biodatas extends Model {
    static associate(models) {
      // Game_Biodatas.hasMany(models.Game_Historys)
      Game_Biodatas.belongsToMany(models.User_Games, { through: "Game_Historys" })
    }
  };
  Game_Biodatas.init({
    name: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Game_Biodatas',
  });
  return Game_Biodatas;
};