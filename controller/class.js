const { User_Games, Class } = require("../models")


class ClassContoller {
  static async createClass(req, res, next) {
    try {
      const { name, User_GameId } = req.body
      const data = await Class.create({
        name, User_GameId
      })
      res.status(201).json(data)
    } catch (error) {

    }
  }

  static async getClasses(req, res, next) {
    try {
      const data = await Class.findAll({
        include: [
          {
            model: User_Games
          }
        ]
      })

      res.status(200).json(data)
    } catch (error) {
      console.log(error)
    }
  }
}

module.exports = ClassContoller