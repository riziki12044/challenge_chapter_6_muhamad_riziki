const { User_Games, Vehicles, Class, Game_Biodatas, Game_BiodatasUser_Games } = require("../models")


class User_GamesController {
  static async createUser_Game(req, res, next) {
    try {
      const { Name, Age, FullName } = req.body
      const _payload = {
        Name,
        Age,
        FullName
      }

      const data = await User_Games.create(_payload)
      // console.log(data.datValues.Name, "<<<< DATA CREATE")
      res.status(201).json(data)
    } catch (error) {
      console.log(error)
    }

  }

  static async getUser_Games(req, res, next) {
    try {
      const data = await User_Games.findAll({
        include: [
          {
            model: Vehicles
          },
          {
            model: Class
          },
          {
            model: Game_Biodatas // relasinya pakai belongsToMany
          }
          // {
          //   model: Game_BiodatasUser_Games,
          //   include: [
          //     {
          //       model: Game_Biodatas
          //     }
          //   ]
          // }  // relasinya one to many ke conjunction
        ]
      })
      res.status(200).json(data)
    } catch (error) {
      console.log(error)
    }

  }

  static async getUser_Game(req, res, next) {
    try {
      const id = req.params.id
      const data = await User_Games.findOne({
        where: {
          id: id
        },
        include: [
          {
            model: Vehicles
          },
          {
            model: Class
          },
          {
            model: Game_Biodatas
          }
        ]
      })
      console.log(data)
      res.status(200).json(data)
    } catch (error) {
      console.log(error)
    }
  }

  static async editUser_Game(req, res, next) {
    try {
      const { Name, Age, FullName } = req.body
      const _payload = {
        Name,
        Age,
        FullName
      }
      const id = req.params.id
      const data = await User_Games.update(_payload, {
        where: {
          id
        },
        returning: true
      })
      console.log(data)
      res.status(201).json(data[1][0])
    } catch (error) {
      console.log(error)
    }
  }

  static async deleteUser_Game(req, res, next) {
    try {
      const id = req.params.id
      const data = await User_Games.destroy({
        where: {
          id
        }
      })
      if (data) {
        res.status(200).json({
          msg: "Succes to delete"
        })
      } else {
        res.status(404).json({
          msg: "Fail to delete"
        })
      }
    } catch (error) {
      console.log(error)
    }
  }
}

module.exports = User_GamesController