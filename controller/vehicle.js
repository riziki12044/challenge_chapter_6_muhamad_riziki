const { User_Games, Vehicles } = require("../models")


class VehiclesController {
  static async createVehicle(req, res, next) {
    try {
      const { Name, User_GameId } = req.body
      const data = await Vehicles.create({ Name, User_GameId })
      res.status(201).json(data)
    } catch (error) {
      console.log(error)
    }
  }

  static async getVehicles(req, res, next) {
    const data = await Vehicles.findAll({
      include: [
        {
          model: User_Games
        }
      ]
    })

    res.status(200).json(data)
  }
}

module.exports = VehiclesController