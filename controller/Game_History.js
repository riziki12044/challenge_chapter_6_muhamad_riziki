const { Game_Historys, User_Games } = require("../models")


class Game_HistorysController {
  static async createGame_Historys(req, res, next) {
    try {
      const { Game_BiodataId, User_GameId } = req.body
      const data = await Game_Historys.create({ Game_BiodataId, User_GameId })
      res.status(201).json(data)
    } catch (error) {
      console.log(error)
    }
  }

  static async getGame_Historys(req, res, next) {
    try {
      const data = await Game_Historys.findAll()
      console.log(data)
      res.status(200).json(data)
    } catch (error) {
      console.log(error)
    }

  }
}

module.exports = Game_HistorysController