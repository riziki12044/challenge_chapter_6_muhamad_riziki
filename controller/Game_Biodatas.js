const { User_Games, Game_Biodatas } = require("../models")


class Game_BiodatasController {
  static async createGame_Biodatas(req, res, next) {
    try {
      const { name } = req.body
      const data = await Game_Biodatas.create({ name })
      res.status(201).json(data)
    } catch (error) {
      console.log(error)
    }
  }

  static async getGame_Biodatas(req, res, next) {
    const data = await Game_Biodatas.findAll()
    res.status(200).json(data)
  }
}

module.exports = Game_BiodatasController