const User_GameController = require("./User_Game")
const ClassController = require("./class")
const VehiclesController = require("./vehicle")
const Game_BiodatasController = require("./Game_Biodatas")
const Game_HistorysController = require("./Game_History")


module.exports = {
  User_GameController,
  ClassController,
  VehiclesController,
  Game_BiodatasController,
  Game_HistorysController
}