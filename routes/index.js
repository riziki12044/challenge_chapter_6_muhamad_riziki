const route = require('express').Router()
const { User_GameController, ClassController, VehiclesController, Game_BiodatasController, Game_HistorysController } = require('../controller')

route.post("/User_Game", User_GameController.createUser_Game)
route.get("/User_Game", User_GameController.getUser_Games)
route.get("/User_Game/:id", User_GameController.getUser_Game)
route.put("/User_Game/:id", User_GameController.editUser_Game)
route.delete("/User_Game/:id", User_GameController.deleteUser_Game)

route.post("/class", ClassController.createClass)
route.get("/class", ClassController.getClasses)

route.post("/vehicle", VehiclesController.createVehicle)
route.get("/vehicle", VehiclesController.getVehicles)

route.post("/Game_Biodata", Game_BiodatasController.createGame_Biodatas)
route.get("/Game_Biodata", Game_BiodatasController.getGame_Biodatas)

route.post("/Game_Historys", Game_HistorysController.createGame_Historys)
route.get("/Game_Historys", Game_HistorysController.getGame_Historys)


module.exports = route

